# Intro_to_Bootstrap_3

In this RMD file you can have a look at other exercises using bootstrap method. You don't need to load any data because we use the ones from different libraries (faithfull, mtcars, ...). Here are the different questions : 


**Q1** Estimate the median time of an eruption and give a confidence interval at 90% on the median.

**Q2** Estimate $P(|\bar{x} - \mu| > 5|\mu)$ where `x` represents an eruption time.

**Q3** : Estimate $\lambda$ and give a parametric bootstrap confidence interval at 95% for $\lambda$. 

**Q4** Calculate the linear correlation coefficient between two variables of the dataset. Realize a significative test of that correlation thanks to `cor.test` function. Comment the result.

**Q5** Give a bootstrap estimator of the correlation variance for the dataset. Suggest a solution "by hand".

**Q6** By using the previous results, tell if the linear correlation coefficient is significantly different from $0$.

**Q7** Explain what does the previous code. For what `sigma_hat`, `res_stu_lm` and `X_i_hat_beta` are used for?

**Q8** Use a re sampling approach of the case sampling individuals to obtain the confidence intervals on the coefficients.

**Q9** Remind the basic approach to test the benefit variable in a linear regression model.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

April 2023





